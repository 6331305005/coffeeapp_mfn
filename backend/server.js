var express = require('express');
var cors = require('cors');
var app = express();
var mysql = require('mysql');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors({ origin: true }));
app.use(cors());

const dbConn = mysql.createConnection({
    host: 'se.mfu.ac.th',
    user: 'md22chayapa',
    password: '6331305005',
    database: 'md22_chayapa_db'
});

dbConn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});

app.listen(process.env.PORT || 7605, function() {
    console.log('Node app is running on 7605');
});

//http://selab.mfu.ac.th:7605/hello
//Testing API Server
app.get('/hello', function(req, res, next) {
    return res.send({ success: true, message: 'hello' })
});
