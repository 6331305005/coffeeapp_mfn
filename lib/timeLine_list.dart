import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mfn_coffee/widget/bottom_nav_bar.dart';
import 'package:mfn_coffee/widget/tasks.dart';

import 'components/dialog_profile.dart';

class TimeLine extends StatelessWidget {
  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return DialogProfile();
        });
  }

  const TimeLine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      backgroundColor: Colors.white,
      appBar: _buildAppBar(context),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(15),
            child: Text(
              'Process',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
          ),
          Expanded(
            child: Tasks(),
          )
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      //   elevation: 0,
      //   backgroundColor: Colors.black,
      //   onPressed: () {},
      //   child: Icon(
      //     Icons.add,
      //     size: 35,
      //   ),
      // ),
    );
  }

  AppBar _buildAppBar(context) {
    return AppBar(
      leading: const BackButton(color: Colors.white),
      title: Text(
        "Timeline Test",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      actions: [
        IconButton(
          onPressed: () {
            createAlertDialog(context);
          },
          icon: CircleAvatar(
            backgroundColor: Colors.white,
            child: Image.asset(
              "assets/acc.png",
              height: 20,
            ),
          ),
        )
      ],
    );
  }
}
