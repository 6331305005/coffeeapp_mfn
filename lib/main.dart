import 'package:flutter/material.dart';
import 'package:mfn_coffee/timeLine_list.dart';
import 'components/dialog_profile.dart';
import 'widget/boxHome.dart';
import 'selectTable.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "HomeScreen",
      home: HomeScreen(),
      theme: ThemeData(
        primarySwatch: Colors.brown,
        fontFamily: 'Raleway',
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return DialogProfile();
        });
  }

  @override
  Widget build(BuildContext context) {
    //this gonna give us total height and with of our device
    return Scaffold(
      appBar: AppBar(
        title: Text("Home",
            style: TextStyle(
                fontSize: 25,
                color: Colors.white,
                fontWeight: FontWeight.bold)),
      ),
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 20.0, horizontal: 25.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Welcome ,Admin",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 30,
                              fontWeight: FontWeight.bold)),
                      IconButton(
                        onPressed: () {
                          createAlertDialog(context);
                        },
                        icon: CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            "assets/acc.png",
                            height: 25,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: GridView.count(
                    padding: EdgeInsets.all(20),
                    crossAxisCount: 2,
                    childAspectRatio: .85,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    children: <Widget>[
                      BoxHome(
                        title: "Process",
                        pic: "assets/pic1.png",
                        press: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return SelectedTable();
                            }),
                          );
                        },
                      ),
                      BoxHome(
                        title: "Tree View",
                        pic: "assets/pic4.png",
                        press: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) {
                              return TimeLine();
                            }),
                          );
                        },
                      ),
                      BoxHome(
                        title: "Report (coming soon)",
                        pic: "assets/pic2.png",
                        press: () {},
                      ),
                      BoxHome(
                        title: "Master (coming soon)",
                        pic: "assets/pic3.png",
                        press: () {},
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
