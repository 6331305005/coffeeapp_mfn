import 'package:flutter/material.dart';
import 'components/dialog_profile.dart';
import 'widget/boxSelect.dart';
import 'widget/bottom_nav_bar.dart';
import 'tableList.dart';

class SelectedTable extends StatelessWidget {
    createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return DialogProfile();
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavBarFb1(),
        appBar: AppBar(
          leading: const BackButton(color: Colors.white),
          title: Text(
            "Seleted Table",
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          actions: [
            IconButton(
              onPressed: () {createAlertDialog(context);},
              icon: CircleAvatar(
                backgroundColor: Colors.white,
                child: Image.asset(
                  "assets/acc.png",
                  height: 20,
                ),
              ),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              boxSelected(
                title: 'เชอรี่',
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return CherryTable();
                    }),
                  );
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              boxSelected(
                title: 'กะลา',
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return PulpingTable();
                    }),
                  );
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              boxSelected(
                title: 'สีกะลา คัดเกรด',
                press: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return GradingTable();
                    }),
                  );
                },
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ));
  }
}
