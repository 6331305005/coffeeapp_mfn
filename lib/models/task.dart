import 'package:flutter/material.dart';

class Task {
  IconData? iconData;
  String? title;
  String? subtitle;
  Color? bgColor;
  Color? iconColor;
  Color? btnColor;
  num? left;
  num? done;
  List<Map<String, dynamic>>? desc;
  bool isLast;
  Task({
    this.iconData,
    this.title,
    this.subtitle,
    this.bgColor,
    this.iconColor,
    this.left,
    this.done,
    this.desc,
    this.btnColor,
    this.isLast = false,
  });
  static List<Task> generateTasks() {
    return [
      Task(
          iconData: Icons.auto_graph_outlined,
          title: 'ยายหน้อย',
          subtitle: 'Honey-red',
          bgColor: Color.fromRGBO(233, 185, 136, 1),
          iconColor: Color.fromARGB(255, 255, 225, 194),
          btnColor: Color.fromRGBO(233, 185, 136, 1),
          done: 1,
          desc: [
            {
              'time': '1',
              'title': 'เชอรี่ HV20210070',
              'slot': '21/02/2002',
              'sub': 'ยายหน้อย',
              'sub2': 'สายพันธุ์: Red',
              'sub3': 'น้ำหนัก: 52.00 Kg',
              'sub4': '',
              'tlColor': Color.fromARGB(255, 109, 27, 27),
              'bgColor': Colors.red[300],
            },
            {
              'time': '2',
              'title': 'กะลา PC20210071',
              'slot': 'กะลาภูผา: เป็น',
              'sub': 'Process: honey-red',
              'sub2': 'ยายหน้อย',
              'sub3': 'น้ำหนักกะลา: 11.00 Kg',
              'sub4': 'วันที่ตาก: 28/02/2022',
              'tlColor': Colors.blue[800],
              'bgColor': Colors.blue[300],
            },
            {
              'time': '3',
              'title': 'สีกะลา BN20210002',
              'slot': 'ยายหน้อย',
              'sub': 'น้ำหนักที่ต้องการสี: 200 kg',
              'sub2': 'สาร AA 200 kg',
              'sub3': 'รวม 200 kg',
              'sub4': '',
              'tlColor': Colors.yellow[700],
              'bgColor': Colors.yellow[200],
            },
            {
              'time': '4',
              'title': '',
              'slot': '',
              'sub': '',
              'sub2': '',
              'sub3': '',
              'sub4': '',
              'tlColor': Colors.grey.withOpacity(0.3),
            },
          ]),
      Task(isLast: true)
    ];
  }
}
