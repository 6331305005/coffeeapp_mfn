import 'package:flutter/material.dart';
import 'package:mfn_coffee/widget/bottom_nav_bar.dart';
import 'package:mfn_coffee/widget/task_title.dart';
import 'package:mfn_coffee/widget/task_timeline.dart';
import 'models/task.dart';

class DetailTimeline extends StatelessWidget {
  final Task task;
  DetailTimeline(this.task);
  @override
  Widget build(BuildContext context) {
    final detailList = task.desc;
    return Scaffold(
        backgroundColor: Colors.brown,
        bottomNavigationBar: BottomNavBarFb1(),
        body: CustomScrollView(
          slivers: [
            _buildAppBar(context),
            SliverToBoxAdapter(
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // DatePicker(),
                    TaskTitle(),
                  ],
                ),
              ),
            ),
            detailList == null
                ? SliverFillRemaining(
                    child: Container(
                        color: Colors.white,
                        child: Center(
                            child: Text(
                          'No task to day',
                          style: TextStyle(color: Colors.grey, fontSize: 18),
                        ))),
                  )
                : SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (_, index) => TaskTimeline(detailList[index]),
                        childCount: detailList.length),
                  )
          ],
        ));
  }

  Widget _buildAppBar(BuildContext context) {
    return SliverAppBar(
      leading: const BackButton(color: Colors.white),
      title: Text(
        "Timeline",
        style: TextStyle(
            fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
      ),
      actions: [
        IconButton(
          onPressed: () {},
          icon: CircleAvatar(
            backgroundColor: Colors.white,
            child: Image.asset(
              "assets/acc.png",
              height: 20,
            ),
          ),
        )
      ],
    );
  }
}
