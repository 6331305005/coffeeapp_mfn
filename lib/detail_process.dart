import 'package:flutter/material.dart';
import 'package:mfn_coffee/tableList.dart';
import 'package:mfn_coffee/widget/form_edit.dart';
import 'package:mfn_coffee/widget/form_info.dart';

class DetailCherryPage extends StatelessWidget {
  const DetailCherryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: FormInfo(
              pic: "assets/pic1.png",
              farmer: 'ยายหน้อย',
              courseName: 'HV20210070',
              weight: '52.00 Kg',
              date: '21/02/2022',
              species: 'Red',
              process: '',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return EditCherryForm(
                      id: 'HV20210070',
                      press: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return CherryTable();
                          }),
                        );
                      },
                    );
                  }),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class DetailPulpingPage extends StatelessWidget {
  const DetailPulpingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: FormInfo(
              pic: "assets/pic1.png",
              farmer: 'ยายหน้อย',
              courseName: 'PC20210071',
              weight: '11.0',
              date: '28/02/2022',
              process: 'Process: Honey-red',
              species: '',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return EditPulpingForm(
                      id: 'PC20210071',
                      press: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return PulpingTable();
                          }),
                        );
                      },
                    );
                  }),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class DetailGradingPage extends StatelessWidget {
  const DetailGradingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: FormInfo(
              pic: "assets/pic1.png",
              farmer: 'ยายหน้อย',
              courseName: '	BN20210002',
              weight: '200.00',
              date: 'Date',
              species: '',
              process: '',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return EditGradingForm(
                      id: 'BN20210002',
                      press: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return GradingTable();
                          }),
                        );
                      },
                    );
                  }),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
