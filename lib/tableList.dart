import 'package:flutter/material.dart';
import 'package:mfn_coffee/widget/item_list.dart';
import 'components/dialog_profile.dart';
import 'widget/bottom_nav_bar.dart';
import 'widget/search_bar.dart';
import 'package:mfn_coffee/detail_process.dart';
import 'components/form.dart';

class CherryTable extends StatelessWidget {
  createAlertDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return DialogProfile();
        });
  }

  const CherryTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // List<Widget> data = [];

    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.white),
        title: Text(
          "Cherry Table",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {
              createAlertDialog(context);
            },
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SearchBar(),
            CommentItem(
              title: 'HV20210070',
              // comment: 'text2',
              subtitle: 'ยายหน้อย',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return DetailCherryPage();
                  }),
                );
              },
            ),
            CommentItem(
                title: 'HV20210077',
                // comment: 'text',
                subtitle: 'ตาตุ้ย',
                press: () {}),
            // Expanded(
            //     child: ListView(
            //   children: <Widget>[
            //     Container(
            //       height: 90,
            //       decoration: BoxDecoration(
            //           gradient: LinearGradient(
            //             colors: [Colors.purple, Colors.red],
            //             begin: Alignment.centerLeft,
            //             end: Alignment.centerRight,
            //           ),
            //           borderRadius: BorderRadius.all(Radius.circular(24))),
            //     )
            //   ],
            // )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return FormCherryCrate();
            }),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class PulpingTable extends StatelessWidget {
  const PulpingTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // List<Widget> data = [];

    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        title: Text(
          "Pulping Table",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SearchBar(),
            CommentItem(
              title: 'PC20210071',
              // comment: 'text2',
              subtitle: 'ยายหน้อย',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return DetailPulpingPage();
                  }),
                );
              },
            ),
            CommentItem(
              title: 'PC20210094',
              // comment: 'text',
              subtitle: 'สวนภูผา',
              press: () {},
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return FormPulpingCrate();
            }),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class GradingTable extends StatelessWidget {
  const GradingTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // List<Widget> data = [];

    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        title: Text(
          "GradingTable",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SearchBar(),
            CommentItem(
              title: 'BN20210002',
              // comment: 'text',
              subtitle: 'ยายหน้อย',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return DetailGradingPage();
                  }),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) {
              return FormGradingCrate();
            }),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
