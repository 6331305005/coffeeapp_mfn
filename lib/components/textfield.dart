import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  final String title;
  const MyTextField({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: title,
      ),
    );
  }
}
