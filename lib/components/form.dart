import 'package:flutter/material.dart';
import '../widget/bottom_nav_bar.dart';
import '../widget/form_create.dart';
import '../widget/form_edit.dart';

class FormCherryCrate extends StatefulWidget {
  const FormCherryCrate({Key? key}) : super(key: key);

  @override
  State<FormCherryCrate> createState() => _FormInfoState();
}

class _FormInfoState extends State<FormCherryCrate> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        title: Text(
          "Create",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: CreateNewCherry(),
    );
  }
}

class FormPulpingCrate extends StatelessWidget {
  const FormPulpingCrate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        title: Text(
          "Create",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: CreateNewPulping(),
    );
  }
}

class FormGradingCrate extends StatelessWidget {
  const FormGradingCrate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        title: Text(
          "Create",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
      body: CreateNewCherry(),
    );
  }
}

class FormEdit extends StatelessWidget {
  const FormEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavBarFb1(),
      appBar: AppBar(
        leading: const BackButton(color: Colors.black),
        title: Text(
          "Edit",
          style: TextStyle(
              fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: CircleAvatar(
              backgroundColor: Colors.white,
              child: Image.asset(
                "assets/acc.png",
                height: 20,
              ),
            ),
          )
        ],
      ),
    );
  }
}
