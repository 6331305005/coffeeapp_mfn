import 'package:flutter/material.dart';

class BoxHome extends StatelessWidget {
  final String title;
  final String pic;
  final VoidCallback press;

  const BoxHome({
    Key? key,
    required this.title,
    required this.pic,
    required this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(13),
      child: Container(
        // padding: EdgeInsets.all(20),
        decoration: BoxDecoration(
          color: Color.fromRGBO(233, 185, 136, 1),
          borderRadius: BorderRadius.circular(13),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 17),
              blurRadius: 17,
              spreadRadius: -23,
              color: Colors.black,
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: press,
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Spacer(),
                  Image.asset(pic),
                  Spacer(),
                  Text(title, textAlign: TextAlign.center, style: TextStyle())
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
