import 'package:flutter/material.dart';
import 'package:mfn_coffee/components/textfield.dart';

import '../tableList.dart';

class EditCherryForm extends StatelessWidget {
  final String id;
  final VoidCallback press;

  const EditCherryForm({Key? key, required this.id, required this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButton(color: Colors.black),
          title: Text(
            "Edit",
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: CircleAvatar(
                backgroundColor: Colors.white,
                child: Image.asset(
                  "assets/acc.png",
                  height: 20,
                ),
              ),
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "รหัสเชอร์รี่",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    id,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              MyTextField(
                title: "วันที่เริ่ม",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "ผู้เก็บเกี่ยว",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "สวน",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "สายพันธุ์",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "น้ำหนัก",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "รายละเอียด",
              ),
              SizedBox(
                height: 35,
              ),
              DecoratedBox(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25), color: Colors.brown
                    // gradient: const LinearGradient(colors: [
                    //   Color(0xFF5D4037),
                    //   Color(0xffA1887F)
                    // ])
                    ),
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all(0),
                      alignment: Alignment.center,
                      padding: MaterialStateProperty.all(const EdgeInsets.only(
                          right: 125, left: 125, top: 15, bottom: 15)),
                      backgroundColor:
                          MaterialStateProperty.all(Colors.transparent),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                      )),
                  onPressed: press,
                  child: Text(
                    "Edit",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

class EditPulpingForm extends StatelessWidget {
  final String id;
  final VoidCallback press;

  const EditPulpingForm({Key? key, required this.id, required this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButton(color: Colors.black),
          title: Text(
            "Edit",
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: CircleAvatar(
                backgroundColor: Colors.white,
                child: Image.asset(
                  "assets/acc.png",
                  height: 20,
                ),
              ),
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "รหัสกะลา",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    id,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              MyTextField(
                title: "Process",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "สายพันธ์ุ",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "วันรับซื้อ",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "ผู้เก็บเกี่ยว",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "สวน",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "รายละเอียด",
              ),
              SizedBox(
                height: 35,
              ),
              DecoratedBox(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25), color: Colors.brown
                    // gradient: const LinearGradient(colors: [
                    //   Color(0xFF5D4037),
                    //   Color(0xffA1887F)
                    // ])
                    ),
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all(0),
                      alignment: Alignment.center,
                      padding: MaterialStateProperty.all(const EdgeInsets.only(
                          right: 125, left: 125, top: 15, bottom: 15)),
                      backgroundColor:
                          MaterialStateProperty.all(Colors.transparent),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                      )),
                  onPressed: press,
                  child: Text(
                    "Edit",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}

class EditGradingForm extends StatelessWidget {
  final String id;
  final VoidCallback press;

  const EditGradingForm({Key? key, required this.id, required this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: const BackButton(color: Colors.black),
          title: Text(
            "Edit",
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: CircleAvatar(
                backgroundColor: Colors.white,
                child: Image.asset(
                  "assets/acc.png",
                  height: 20,
                ),
              ),
            )
          ],
        ),
        body: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "รหัสคัดเกรด",
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    id,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(
                height: 10,
              ),
              MyTextField(
                title: "AA",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "A",
              ),
              SizedBox(
                height: 20,
              ),
              MyTextField(
                title: "Peabrry",
              ),
              SizedBox(
                height: 20,
              ),
              // MyTextField(
              //   title: "สายพันธุ์",
              // ),
              // SizedBox(
              //   height: 20,
              // ),
              // MyTextField(
              //   title: "น้ำหนัก",
              // ),
              // SizedBox(
              //   height: 20,
              // ),
              // MyTextField(
              //   title: "รายละเอียด",
              // ),
              // SizedBox(
              //   height: 35,
              // ),
              DecoratedBox(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25), color: Colors.brown
                    // gradient: const LinearGradient(colors: [
                    //   Color(0xFF5D4037),
                    //   Color(0xffA1887F)
                    // ])
                    ),
                child: ElevatedButton(
                  style: ButtonStyle(
                      elevation: MaterialStateProperty.all(0),
                      alignment: Alignment.center,
                      padding: MaterialStateProperty.all(const EdgeInsets.only(
                          right: 125, left: 125, top: 15, bottom: 15)),
                      backgroundColor:
                          MaterialStateProperty.all(Colors.transparent),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                      )),
                  onPressed: press,
                  child: Text(
                    "Edit",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
