import 'package:flutter/material.dart';

class CommentItem extends StatelessWidget {
  const CommentItem({
    Key? key,
    required this.title,
    required this.subtitle,
    // required this.comment,
    required this.press,
    this.profileImgUrl,
  }) : super(key: key);

  final String title;
  final String subtitle;
  // final String comment;
  final String? profileImgUrl;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CircleAvatar(
            foregroundImage:
                NetworkImage(profileImgUrl != null ? profileImgUrl! : ""),
            child: Icon(Icons.account_circle)),
        SizedBox(width: 10),
        Expanded(
          flex: 7,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                subtitle,
              ),
              SizedBox(height: 30),
              // Text(comment,
              //     style: TextStyle(color: Colors.black87, fontSize: 12))
            ],
          ),
        ),
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(
              icon: Icon(Icons.navigate_next),
              onPressed:press,
            ),
          ],
        ),
      ],
    );
  }
}
