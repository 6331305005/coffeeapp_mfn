import 'package:flutter/material.dart';
import 'package:mfn_coffee/components/textfield.dart';

import '../tableList.dart';

class CreateNewCherry extends StatelessWidget {
  const CreateNewCherry({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          MyTextField(
            title: "วันที่เริ่ม",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "ผู้เก็บเกี่ยว",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "สวน",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "สายพันธุ์",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "น้ำหนัก",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "รายละเอียด",
          ),
          SizedBox(
            height: 50,
          ),
          DecoratedBox(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25), color: Colors.brown
                // gradient: const LinearGradient(colors: [
                //   Color(0xFF5D4037),
                //   Color(0xffA1887F)
                // ])
                ),
            child: ElevatedButton(
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all(0),
                  alignment: Alignment.center,
                  padding: MaterialStateProperty.all(const EdgeInsets.only(
                      right: 125, left: 125, top: 15, bottom: 15)),
                  backgroundColor:
                      MaterialStateProperty.all(Colors.transparent),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                  )),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return CherryTable();
                  }),
                );
              },
              child: Text(
                "Create",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}

class CreateNewPulping extends StatelessWidget {
  const CreateNewPulping({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          MyTextField(
            title: "ID",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "Process",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "ผู้เก็บเกี่ยว",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "สวน",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "สถานะ",
          ),
          SizedBox(
            height: 50,
          ),
          DecoratedBox(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25), color: Colors.brown
                // gradient: const LinearGradient(colors: [
                //   Color(0xFF5D4037),
                //   Color(0xffA1887F)
                // ])
                ),
            child: ElevatedButton(
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all(0),
                  alignment: Alignment.center,
                  padding: MaterialStateProperty.all(const EdgeInsets.only(
                      right: 125, left: 125, top: 15, bottom: 15)),
                  backgroundColor:
                      MaterialStateProperty.all(Colors.transparent),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                  )),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return CherryTable();
                  }),
                );
              },
              child: Text(
                "Create",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}

class CreateNewGrading extends StatelessWidget {
  const CreateNewGrading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: <Widget>[
          MyTextField(
            title: "ID",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "ผู้เก็บเกี่ยว",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "สวน",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "วันที่เริ่มต้น",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "ถึง",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "วันที่เก็บเกี่ยว",
          ),
          SizedBox(
            height: 20,
          ),
          MyTextField(
            title: "ถึง",
          ),
          SizedBox(
            height: 50,
          ),
          DecoratedBox(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(25), color: Colors.brown
                // gradient: const LinearGradient(colors: [
                //   Color(0xFF5D4037),
                //   Color(0xffA1887F)
                // ])
                ),
            child: ElevatedButton(
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all(0),
                  alignment: Alignment.center,
                  padding: MaterialStateProperty.all(const EdgeInsets.only(
                      right: 125, left: 125, top: 15, bottom: 15)),
                  backgroundColor:
                      MaterialStateProperty.all(Colors.transparent),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                  )),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return CherryTable();
                  }),
                );
              },
              child: Text(
                "Create",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    ));
  }
}
