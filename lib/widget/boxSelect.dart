import 'package:flutter/material.dart';

class boxSelected extends StatelessWidget {
  final String title;
  final VoidCallback press;
  const boxSelected({Key? key, required this.title, required this.press})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          color: Color.fromRGBO(233, 185, 136, 1),
          borderRadius: BorderRadius.circular(10)),
      height: 75,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold)),
          IconButton(
            onPressed: press,
            icon: CircleAvatar(
              backgroundColor: Color.fromRGBO(233, 185, 136, 1),
              child: Image.asset("assets/right.png", height: 70),
            ),
          )
        ],
      ),
    );
  }
}
